Android上的菜单展示风格目前是各式各样的，但用的最多且体验最好的莫过于左右滑动来显示隐藏的菜单
本示例实现了各种方式的菜单展示效果，只有你想不到的
源码：https://github.com/SimonVT/android-menudrawer.git